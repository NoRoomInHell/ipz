﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IPZ_LAB_3
{
    
   public class NewCase
    {
      private  List<Witnesses> W_lsit = new List<Witnesses>();
      
     private   List<Suspect> S_list = new List<Suspect>();
     private   List<Evidence> E_list = new List<Evidence>();
        public long currentIdCase{get;set;}
        public static Dictionary<long, NewCase> NCDic = new Dictionary<long, NewCase>();

        public virtual List<Witnesses> Witnesse_List
        {
            get { return W_lsit; }
            set { W_lsit = value; }
        }
        public virtual List<Suspect> Suspect_List
        {
            get { return S_list; }
            set { S_list = value;}
        }
        public virtual List<Evidence> Evidence_List
        { get { return E_list; }
           set { E_list = value; }
        }
       
        public NewCase()
        {

            NewCase.NCDic.Add(new CaseID().ID, this);
            currentIdCase = CaseID.idStart;
               
        }

        
        public void AddWitnesse(Witnesses ws)
        {
            Witnesse_List.Add(ws);
       //     ws.IDHolder(currentIdCase);
            
        }
        public void AddSuspect(Suspect ss)
        {
            Suspect_List.Add(ss);
           // ss.IDHolder(currentIdCase);
        }
        public void AddEvidence(Evidence es)
        {
            Evidence_List.Add(es);
            //es.IDHolder(currentIdCase);

        }
        public void Remove(Witnesses ws)
        {
           Witnesse_List.Remove(ws);

        }
        public void Remove(Suspect ws)
        {
          Suspect_List.Remove(ws);

        }
        public void Remove(Evidence ws)
        {
           Evidence_List.Remove(ws);

        }
        
        
        
       
    }
}
