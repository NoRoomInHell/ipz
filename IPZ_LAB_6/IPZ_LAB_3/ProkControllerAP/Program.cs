﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.ProkControllerAP
{
    class Program
    {
        static void Main(string[] args)
       {
            
           //Witnesses w1 = new Witnesses(new Name { FirstName = "Jan", LastName = "Van Dam" }, new Address { City = "KV",House = " 33",Street = "Novgor" });
           //Witnesses w2 = new Witnesses(new Name { FirstName = "Nah", LastName = "Mad Nav" }, new Address { City = "KV", House = " 36", Street = "Rogvon" });
           //Witnesses w3 = new Witnesses(new Name { FirstName = "Jan", LastName = "Van33 Dam" }, new Address { City = "K323V", House = " 43433", Street = "Novgfdsfdor" });
           //Suspect s1 = new Suspect(new Name { FirstName = "Andrew", LastName = " Tarasenko" }, new Address { City = "Kiev", Street = "Lenina", House = "23" });
           //Suspect s2 = new Suspect(new Name { FirstName = "Rostik", LastName = " Chyprin" }, new Address { City = "Rostov", Street = "Derevyanko", House = "66" });
           //Suspect s3 = new Suspect(new Name { FirstName = "Chilo", LastName = " Tarasenko" }, new Address { City = "Ostana", Street = "Pioner", House = "66" });
           //Evidence e1 = new Evidence(new EvidenceInfo { EvidneceDetail = "Clother with strange color" });
           //Evidence e2 = new Evidence(new EvidenceInfo { EvidneceDetail = "Steps from boots" });
           //Evidence e3 = new Evidence(new EvidenceInfo { EvidneceDetail = "Here no one evidence, just text" });
           // NewCase ns = new NewCase();
           // ns.AddWitnesse(w1);
           // ns.AddSuspect(s1);
           // ns.AddEvidence(e1);
           // ns.AddWitnesse(w2);
           // ns.AddSuspect(s2);
           // ns.AddEvidence(e2);
           // NewCase na = new NewCase();
           // na.AddWitnesse(w3);
           // na.AddSuspect(s3);
           // na.AddEvidence(e3);
          //  FindCase f = new FindCase();
           // f.GetAllVal(2);
           // f.GetAllVal(1);
          //  f.GetAllVal();
           // f.GetAllVal(w3);
           // f.GetAllVal(s3);
            //ProkDbTest.Save(NS);
            //ProkDbTest.AddWitnesse(ws);
            //ProkDbTest.AddWitnesse(wss);
            //ProkDbTest.AddSuspect(sp);
            //ProkDbTest.AddSuspect(spp);
            //ProkDbTest.Restore();
         // ------------------------------------------------------------------------------------
           CreateCase();
           CreateEvidence();
           CreateSuspect();
           CreateWitnesse();
           AddNewWitnesseToCase();
           AddNewSuspectToCase();
           AddNewEvidenceToCase();
           AddWitnesseFromDataBase();
           AddSuspectFromDataBase();
           AddEvidenceFromDataBase();
           DisplayWitnesses();
           DisplaySuspects();
           DisplayEvidence();
           DisplayCase();
     
        }
        private static void AddName(INameController nameController)
        {
            nameController.CreateName("ff","ads");
        
        }
        private static void AddWitnesse(IWitnesseController witnesController)
        {
          
            witnesController.CreateNewWitnesse("TESTNAME", "TESTLASTNAME");
            witnesController.CreateNewWitnesse("LOL", "RIOT");
            witnesController.CreateNewWitnesse("TEST 3", " 3 TEST");
            witnesController.CreateNewWitnesse("TEST 4 ", "4 TEST");
            witnesController.CreateNewWitnesse("TEST 5", "5 TEST");
            witnesController.CreateNewWitnesse("TEST 6", "6 TEST");
        }
        private static void AddEvidence(IEvidenceController evidenceController)
        {
            evidenceController.CreateEvidence("Some info 1", "Some facts and detail 1", true);
            evidenceController.CreateEvidence("Some info 2", "Some facts and detail 2",false);
            evidenceController.CreateEvidence("Some info 3", "Some facts and detail 3", true);
        }
        private static void AddSuspect(ISuspectController susController)
        {

            susController.CreateNewSuspect("SuspectName1", "SuspectLastName1");
            susController.CreateNewSuspect("SuspectName2", "SuspectLastName2");
            susController.CreateNewSuspect("SuspectName3", "SuspectLastName3");
            susController.CreateNewSuspect("SuspectName4", "SuspectLastName4");
        
        }

        private static void AddNewCase(IMenuController menuController)
        {
            menuController.CreateCase();
               
        }
        private static void AddEvidenceFromData(IMenuController menu)
        {
            menu.AddEvidenceFrom(1, 1);
            menu.AddEvidenceFrom(1, 2);
            menu.AddEvidenceFrom(1, 3);
            }
        private static void AddWitnesseFromData(IMenuController menu)
        {
            menu.AddWitnesseFrom(2, 4);
            menu.AddWitnesseFrom(2, 5);
            menu.AddWitnesseFrom(2, 6);
        }
        private static void AddSuspectFromData(IMenuController menu)
        {
            menu.AddSuspectFrom(1, 1);
            menu.AddSuspectFrom(1, 2);
        }
        private static void AddWitnesseToNewCase(IMenuController menu)
        {
            menu.AddWitnesse("NEWNAME", "NEWLASTNAME", 2);
        
            menu.AddWitnesse("NEWNAME2", "NEWLASTNAME2", 2);
        
        }
        private static void AddSuspectToNewCase(IMenuController menu)
        {
            menu.AddSuspect("SUSPECTNEWNAME", "SUSPECTNEWLASTNAME", 1);

        }
        private static void AddEvidenceToNewCase(IMenuController menu)
        {
            menu.AddEvidence("INFO", "DETAIL", false, 2);
        }
        private static void Rename(IWitnesseController witContr)
        {
            witContr.Rename(9, "NEWNAMEFORMCONT");
           
        }
  

        private static void CreateCase()
        {
            using(var menuController = ControllerFactory.MakeMenuController())
            {
              
                AddNewCase(menuController);
               
              
            }
        }
     
        private static void CreateName()
        {
            using (var nameController = ControllerFactory.MakeNameController())
            {
                
                AddName(nameController);
               
            }
        }
        private static void CreateWitnesse()
        {
            using(var witController = ControllerFactory.MakeWitnesseController())
            {
              AddWitnesse(witController);
               

             
            }
        }
        private static void CreateSuspect()
        {
            using(var susController = ControllerFactory.MakeSuspectController())
            {
                AddSuspect(susController);
            }
        }
        private static void CreateEvidence()
        {

            using(var evController = ControllerFactory.MakeEvidenceController())
            {
                AddEvidence(evController);
            }
        }
        private static void AddNewSuspectToCase()
        {
            using (var menuContr= ControllerFactory.MakeMenuController())
            {
                AddSuspectToNewCase(menuContr);
            }
        }
        private static void AddNewWitnesseToCase()
        {
            using (var mentContr = ControllerFactory.MakeMenuController())
            {
                AddWitnesseToNewCase(mentContr);
            }

        }
        private static void AddNewEvidenceToCase()
        {
            using (var menuContr = ControllerFactory.MakeMenuController())
            {
                AddEvidenceToNewCase(menuContr);
            }
        }
        private static void AddSuspectFromDataBase()
        {
            using(var menuContr = ControllerFactory.MakeMenuController())
            {
                AddSuspectFromData(menuContr);
            }
        }
        private static void AddWitnesseFromDataBase()
        {
            using(var mentContr = ControllerFactory.MakeMenuController())
            {
                AddWitnesseFromData(mentContr);
            }
        }
        private static void AddEvidenceFromDataBase()
        {
            using (var menuContr = ControllerFactory.MakeMenuController())
            {
                AddEvidenceFromData(menuContr);
            }
        }
        private static void Rename()
        {
            using(var wintContr = ControllerFactory.MakeWitnesseController())
            {
                Rename(wintContr);
            }
        }
            private static void DisplayWitnesses()
        {
           using(var witController = ControllerFactory.MakeWitnesseController())
           {
               
               ReportGenerator generator = new ReportGenerator();
               generator.DisplayWitnesses(witController.GetAllWitnesses());
               
               
           }
        }
        private static void DisplaySuspects()
            {
            using(var susController = ControllerFactory.MakeSuspectController())
            {
                ReportGenerator generator = new ReportGenerator();
                generator.DisplaySuspect(susController.GetAllSuspects());
               
            }
            }
        private static void DisplayEvidence()
        {
            using (var evController = ControllerFactory.MakeEvidenceController())
            {
                ReportGenerator generator = new ReportGenerator();
                generator.DisplayEvidence(evController.GetAllEvidence());

            }
        }
        private static void DisplayCase()
        {
            using (var menuController  = ControllerFactory.MakeMenuController())
            {
              
                ReportGenerator generator = new ReportGenerator();
            generator.DisplaySingleCase(menuController.OpenCase(1));
            generator.DisplaySingleCase(menuController.OpenCase(2));
            generator.DisplayCases(menuController.GetAllCases());
             
            }
        }
        
    }
}
