﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;

namespace IPZ_LAB_3.ProkControllerAP
{
    class ReportGenerator
    {
        public void DisplayWitnesses(Witnesses[] witnesss)
        {
            Console.WriteLine("ALL WITNESSES: ");
            Console.WriteLine(("".PadRight(15, '-')));
         foreach(Witnesses s in witnesss)
         {
           
             Console.WriteLine(s.firstName +" "+ s.lastName + " " +  "Wintesse current ID is " + s.currentIdCase);
         }
        
         if (witnesss == null)
             throw new SystemException("WITNESSE NOT FOUND");
         
            
        }
        public void DisplayEvidence(Evidence[] evidence)
        {
            Console.WriteLine("ALL EVIDENCE: ");
            Console.WriteLine(("".PadRight(15, '-')));
            foreach(Evidence ev in evidence)
            {
              
                Console.WriteLine(ev.information + " " + ev.evidenceType + " " + "Evidence current ID is " + ev.currentIdCase);
            }
          
            if (evidence == null)
                throw new SystemException("EVIDENCE NOT FOUND");
        }
        public void DisplaySuspect(Suspect[] suspects)
        {
            Console.WriteLine("ALL SUSPECTS: ");
            Console.WriteLine(("".PadRight(15, '-')));
            foreach(Suspect s in suspects)
            {
               
                Console.WriteLine(s.firstName + " " + s.lastName + " " + "Suspect current ID is " + s.currentIdCase);
                   
            }
            
            if (suspects == null)
                throw new SystemException("SUSPECT NOT FOUND");
        }
        public void DisplayCases(NewCase[] cases)
        {
            Console.WriteLine("ALL CASES: ");
               foreach( NewCase nc in cases)
               {
                   Console.WriteLine(("".PadRight(15, '-')));
                   Console.WriteLine("There is " + cases.Length.ToString() + " Cases");

                   Console.WriteLine("Case with ID: " +""+ nc.currentIdCase.ToString());
                   foreach(Witnesses w in nc.Witnesse_List)
                   {
                       Console.WriteLine("Witnesses : " +"\n" +  w.firstName + "" + w.lastName);
                       
                   }
                    foreach( Suspect s in nc.Suspect_List)
                    {
                        Console.WriteLine("Suspects : " + "\n" + s.firstName + "" + s.lastName); 
                    }
                   foreach(Evidence ev in nc.Evidence_List)
                   {
                       Console.WriteLine("Evidence : " + "\n" + ev.information + "" + ev.evidenceType);
                   }
                  
               }
            
        }
        public void DisplaySingleCase(NewCase ns)
    {

        if (ns == null)
            throw new SystemException("THIS CASE NOT FOUND" );

        Console.WriteLine("IN CASE WITH ID: " + ns.currentIdCase + " " + ns.Witnesse_List.Count + "" + " Witnesses");
        Console.WriteLine(("".PadRight(15, '-')));
            foreach(Witnesses s in ns.Witnesse_List)
            {
                
                Console.WriteLine("{0}", s.firstName + "" + s.lastName+ 
                   "\n" +" His ID is :" + "" + s.currentIdCase);
                
            }
            Console.WriteLine("IN CASE WITH ID: "+ ns.currentIdCase + " " + ns.Suspect_List.Count + "" + " Suspects");
            Console.WriteLine(("".PadRight(15, '-')));
            foreach(Suspect s in ns.Suspect_List)
            {
                
                Console.WriteLine("{0}", s.firstName + "" + s.lastName + "\n" + " His ID is :" + ""
                    + s.currentIdCase);
            }
            Console.WriteLine("IN CASE WITH ID: " + ns.currentIdCase + " " + ns.Evidence_List.Count + "" + "Evidence");
            Console.WriteLine(("".PadRight(15, '-')));
            foreach(Evidence ev in ns.Evidence_List)
            {
               
                Console.WriteLine("{0}", ev.information + "" + ev.evidenceType + "\n" + "His ID is :" + "" +
                    ev.currentIdCase);
            }

           
       }
            
            

    }
       
    
    
}
