﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_LAB_3
{
   public class Evidence
    {
       public long currentIdCase { get; set; }
       public string information { get; set; }
       public string evidenceType { get; set; }
       public bool flag { get; set; }
        
        public Evidence()
        {
            EvidenceInfo info = new EvidenceInfo();
            this.information = info.EvidneceDetail;
            this.flag = info.isPhysical;
            this.evidenceType = info.EvidenceTypeInfo;
            IDHolder(currentIdCase);
        }
        public long IDHolder(long id)
        {
            currentIdCase = id;
            return currentIdCase;
        }
        public void ShowFullInfo()
        {
            Console.WriteLine("Evidence in case: " + information);
            Console.WriteLine("CaseID is : " + currentIdCase);
        }

    }
}
