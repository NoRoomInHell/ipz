﻿
namespace IPZ_LAB_3.ProkController
{
   public static class ControllerFactory
    {
       public static IWitnesseController MakeWitnesseController()
       {
           return new WitnesseController();
       }
       public static INameController MakeNameController()
       {
           return new NameController();
       }
       public static IMenuController MakeMenuController()
       {

           return new MenuController();
       }
       public static ISuspectController MakeSuspectController()
       {
           return new SuspectController();
       }
       public static IEvidenceController MakeEvidenceController()
       {
           return new EvidenceController();
       }
       }
}
