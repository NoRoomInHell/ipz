﻿using System;
using IPZ_LAB_3;
namespace IPZ_LAB_3.ProkController
{
  public  interface IMenuController:IDisposable
    {
      void CreateCase();

      NewCase OpenCase(long id);

      NewCase[] GetAllCases();

      void AddWitnesse(string firstName,string lastName,long id);
      void AddWitnesseFrom( long id,long witnesseId);
      void AddSuspect(string firstName, string lastName, long id);
      void AddSuspectFrom(long id, long suspectId);
      void AddEvidence(string info, string typeDetail, bool isPhysic,long id);
      void AddEvidenceFrom(long id, long evidenceId);
  }
}
