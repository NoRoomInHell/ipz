﻿using System;
using IPZ_LAB_3;
namespace IPZ_LAB_3.ProkController
{
   public interface IEvidenceController:IDisposable
    {
       Evidence[] GetAllPhysicalEvidence(bool flag );
       Evidence[] GetAllNonPhysicalEvidence(bool flag);
       Evidence[] GetAllEvidence();

       Evidence FindByID(long ID);
       void CreateEvidence(string info, string detail, bool isPhysical);
       void Delete(long id);
       void DeleteAllEvidence();
       void Change(long id, string info, string detail, bool isPhysical);



    }
}
