﻿using System;
using IPZ_LAB_3;
namespace IPZ_LAB_3.ProkController
{
    public interface IWitnesseController: IDisposable
    {
        Witnesses[] GetAllWitnesses();
        Witnesses[] SearchWitnessesByName(string name);

        Witnesses FindWitnesseByName(string name);
        void CreateNewWitnesse(string name, string lastname);
        void Rename(long id, string newName);
        void Delete(long id);
        void DeleteAllWitnesse(); 
      
        
    }
}
