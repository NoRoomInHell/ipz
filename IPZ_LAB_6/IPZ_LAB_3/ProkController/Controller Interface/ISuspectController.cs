﻿using System;
using IPZ_LAB_3;

namespace IPZ_LAB_3.ProkController
{
   public interface ISuspectController: IDisposable
    {
        Suspect[] GetAllSuspects();
        Suspect[] SearchSuspectByName(string name);

       Suspect FindSuspectByName(string name);
        void CreateNewSuspect(string name, string lastname);
        void Rename(long id, string newName);
        void Delete(long id);
        void DeleteAllSuspects(); 
      
        
    }
}
