﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
namespace IPZ_LAB_3.ProkController
{
    class SuspectController:BasicController,ISuspectController
    {
            private ISuspectRepositroy suspectRepository;
      
        public SuspectController()
        {
            this.suspectRepository = RepositoryFactory.MakeSuspectRepositroy(GetDBContext());

        }
        public Suspect[] GetAllSuspects()
   {

       return suspectRepository.LoadAll().ToArray();
   }

        public Suspect[] SearchSuspectByName(string name)
        {

            return suspectRepository.SearchSuspectByName(name).ToArray();

        }
       public Suspect FindSuspectByName(string name)
        {

            return suspectRepository.Load(name);

        }
        public void CreateNewSuspect(string firstName,string lastName)
        {
            IPZ_LAB_3.Suspect ws = new IPZ_LAB_3.Suspect();
            ws.firstName = firstName;
            ws.lastName = lastName;

            suspectRepository.Add(ws);
            suspectRepository.Commit();
            
        }
       public void Rename(long id, string newName)
        {
            suspectRepository.LoadById(id).firstName = newName;
            suspectRepository.Commit();
        }
       public void Delete(long id)
       {
           var db = suspectRepository.LoadById(id);
           suspectRepository.Remove(db);
           suspectRepository.Commit();
       }
       public void DeleteAllSuspects()
       {
          suspectRepository.DeleteAll();
           suspectRepository.Commit();
       }
    

    }
}
