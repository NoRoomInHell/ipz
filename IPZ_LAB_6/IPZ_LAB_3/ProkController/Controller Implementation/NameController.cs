﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
namespace IPZ_LAB_3.ProkController
{
    class NameController:BasicController,INameController
    {
        private INameRepository nameController;
     
        public NameController()
        {
            this.nameController = RepositoryFactory.MakeNamesRepository(GetDBContext());
        }
       public  Name[] GetAllNames()
        {
            return nameController.LoadAll().ToArray();
        }
        public Name[] FindByName(string name)
       {
           return nameController.FindByName(name).ToArray();

       }
        public void CreateName(string firstName, string lastName)
        {
           IPZ_LAB_3.Name nam = new IPZ_LAB_3.Name();
            nam.FirstName = firstName;
            nam.LastName = lastName;
            nameController.Add(nam);
            nameController.Commit();

        }
        
    }
}
