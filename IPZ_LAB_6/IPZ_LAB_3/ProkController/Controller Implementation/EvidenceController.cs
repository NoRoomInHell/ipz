﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
namespace IPZ_LAB_3.ProkController
{
    class EvidenceController:BasicController,IEvidenceController
    {
        public IEvidenceRepository evidenceRepository;
        public EvidenceController()
        {
            this.evidenceRepository = RepositoryFactory.MakeEvidenceRepository(GetDBContext());
        }
        public Evidence FindByID(long ID)
        {
            return evidenceRepository.LoadById(ID);
        }
        public Evidence[] GetAllPhysicalEvidence(bool flag)
        {
            return evidenceRepository.FindByFlagPhysic(flag).ToArray();
        }
        public Evidence[] GetAllNonPhysicalEvidence(bool flag)
        {
            return evidenceRepository.FindByFlagNonPhysic(flag).ToArray();
        }
        public Evidence[] GetAllEvidence()
        {
            return evidenceRepository.LoadAll().ToArray();
        }
        public void SearchEvidenceByID(long id)
        {
            evidenceRepository.LoadById(id);
        }
        public void CreateEvidence(string info,string detailType,bool flagPhysic)
        {
            Evidence ev = new Evidence();
            ev.information = info;
            ev.evidenceType = detailType;
            ev.flag = flagPhysic;
            evidenceRepository.Add(ev);
            evidenceRepository.Commit();
        }
        public void Change(long id,string info,string detailType,bool flagPhysic)
        {

            evidenceRepository.LoadById(id).information = info;
            evidenceRepository.LoadById(id).evidenceType = detailType;
            evidenceRepository.LoadById(id).flag = flagPhysic;
            evidenceRepository.Commit();
                    
        }
        public void Delete(long id)
        {
            var db = evidenceRepository.LoadById(id);
            evidenceRepository.Remove(db);
            evidenceRepository.Commit();
        }
        public void DeleteAllEvidence()
        {
            evidenceRepository.DeleteAll();
            evidenceRepository.Commit();
        }

    }
}
