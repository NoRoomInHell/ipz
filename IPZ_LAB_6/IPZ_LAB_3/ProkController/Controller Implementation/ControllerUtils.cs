﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
namespace IPZ_LAB_3.ProkController
{
    static class ControllerUtils
    {
        public static T TakeObjectById<T>(Orm.IRepository<T> repository, long objectID)
        where T:class
        {
            T result = repository.LoadById(objectID);
            if (result == null)
                throw new SystemException(typeof(T).Name + "#" + objectID + " not found");
            return result;
        }

    }
}
