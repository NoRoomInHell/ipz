﻿using System;
using System.Linq;
using System.Text;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;

namespace IPZ_LAB_3.ProkController
{

   class WitnesseController : BasicController, IWitnesseController
    {
        private IWitnesseRepository witnesseRepository;
      
        public WitnesseController()
        {
            this.witnesseRepository = RepositoryFactory.MakeWitnesseRepository(GetDBContext());

        }
        public Witnesses[] GetAllWitnesses()
   {
           return witnesseRepository.LoadAll().ToArray();
   }

        public Witnesses[] SearchWitnessesByName(string name)
        {
            return witnesseRepository.SearchWitnessesByName(name).ToArray();

        }
       public Witnesses FindWitnesseByName(string name)
        {
          return  witnesseRepository.Load(name);

        }
        public void CreateNewWitnesse(string firstName,string lastName)
        {
            IPZ_LAB_3.Witnesses ws = new IPZ_LAB_3.Witnesses();
            ws.firstName = firstName;
            ws.lastName = lastName;

            witnesseRepository.Add(ws);
            witnesseRepository.Commit();
            
        }
       public void Rename(long id, string newName)
        {
            witnesseRepository.LoadById(id).firstName = newName;
            witnesseRepository.Commit();
        }
       public void Delete(long id)
       {
           var db = witnesseRepository.LoadById(id);
           witnesseRepository.Remove(db);
           witnesseRepository.Commit();
       }
       public void DeleteAllWitnesse()
       {
           witnesseRepository.DeleteAll();
           witnesseRepository.Commit();
       }
    
    }
}
