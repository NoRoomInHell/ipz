﻿using System;
using System.Linq;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
    public interface IMenuRepository: IRepository<NewCase>
    {
    IQueryable<NewCase> FindByID(long id);


    }
}
