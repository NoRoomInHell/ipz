﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_LAB_3.Orm
{
    public interface INameRepository: IRepository<Name>
    {
        IQueryable<Name> FindByName(string name);

    }
}
