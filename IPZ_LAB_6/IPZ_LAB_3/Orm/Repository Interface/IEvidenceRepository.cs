﻿using System;
using System.Linq;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
    public interface IEvidenceRepository:IRepository<Evidence>
    {
        IQueryable<Evidence> FindByFlagPhysic(bool flag);
        IQueryable<Evidence> FindByFlagNonPhysic(bool flag);
    }
}
