﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
 public   interface ISuspectRepositroy:IRepository<Suspect>
    {
        IQueryable<Suspect> SearchSuspectByName(string name);
    }
}
