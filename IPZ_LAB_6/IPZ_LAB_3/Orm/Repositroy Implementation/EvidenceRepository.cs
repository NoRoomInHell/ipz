﻿using System;
using IPZ_LAB_3;
using System.Linq;

namespace IPZ_LAB_3.Orm
{
   class EvidenceRepository:BasicRepository<Evidence>,IEvidenceRepository
    {
       public EvidenceRepository(ProkDbContext dbContext)
           :base(dbContext,dbContext.Evidence)
       { }

       public IQueryable<Evidence> FindByFlagPhysic(bool flag)
       {
           var db = GetDBContext();
           if (flag)
               return db.Evidence.Where(p => p.flag);
           else
               Console.WriteLine("Evidence doesn't exist" + " " + "All Evidence with non physical evidence");
           return db.Evidence.Where(p => !p.flag);
       }
       public IQueryable<Evidence> FindByFlagNonPhysic(bool flag)
       {
           var db = GetDBContext();
           if (flag)
               return db.Evidence.Where(p => !p.flag);
           else
               Console.WriteLine("Evidence doesn't exist" + " " + "All Evidence with  physical evidence");
           return db.Evidence.Where(p => p.flag);
       }
       

    }
}
