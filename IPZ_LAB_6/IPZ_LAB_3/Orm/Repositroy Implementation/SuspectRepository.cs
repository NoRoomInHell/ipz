﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
    class SuspectRepository:BasicRepository<Suspect>,ISuspectRepositroy
    {

        public SuspectRepository(ProkDbContext dbContext)
            : base(dbContext, dbContext.Sus)
        { }

        public IQueryable<Suspect> SearchSuspectByName(string name)
        {
            var db = GetDBContext();
            return db.Sus.Where(p => p.firstName == name);

        }
 
    }
}
