﻿using System;
using IPZ_LAB_3;
using System.Linq;
using System.Data.Entity;
namespace IPZ_LAB_3.Orm
{
    class BasicRepository<T> where T:class
    {
        protected BasicRepository(ProkDbContext dbContext,DbSet<T> dbSet)
        {
            this.dbContext = dbContext;
            this.dbSet = dbSet;

        }
        protected ProkDbContext GetDBContext()
        {

            return this.dbContext;
        }
        protected DbSet GetDBSet()
        {
            return this.dbSet;
        }
        
        public void Add( T obj)
        {
            dbSet.Add(obj);
                
        }
      
        public void Remove(T obj)
        {
            dbSet.Remove(obj);
        }
        public void DeleteAll()
        {
            dbSet.RemoveRange(dbSet);
        }
        public void Commit()
        {
          
            dbContext.ChangeTracker.DetectChanges();
            dbContext.SaveChanges();
        }
        

        public IQueryable<T> LoadAll()
        {
            return dbSet;
        }


        public T Load(string name)
        {
            return dbSet.Find(name);
        }
        public T AttachByID(long id)
        {
            
            return dbSet.Attach(LoadById(id));
        }
        //public T AddtoList(long id)
        //{
        //    var db = dbSet.Find(id);
        //    return dbSet.
        //}
        public T LoadById(long id)
        {
            return dbSet.Find(id);
        }
        private ProkDbContext dbContext;
        private DbSet<T> dbSet;
    }
}
