﻿using System;
using System.Linq;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
    class WitnesseRepository:BasicRepository<Witnesses>,IWitnesseRepository
    {
        public WitnesseRepository(ProkDbContext dbContext)
            : base(dbContext,dbContext.Wit)
        { }

        public IQueryable<Witnesses> SearchWitnessesByName(string name)
        {
            var db = GetDBContext();
            return db.Wit.Where(p => p.firstName == name);

        }
      
            
       
        }
}
