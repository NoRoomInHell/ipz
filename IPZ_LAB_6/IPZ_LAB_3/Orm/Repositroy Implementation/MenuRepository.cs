﻿using System;
using IPZ_LAB_3;
using System.Linq;
namespace IPZ_LAB_3.Orm
{
    class MenuRepository:BasicRepository<NewCase>,IMenuRepository
    {
        public MenuRepository(ProkDbContext dbContext)
            :base(dbContext,dbContext.NewCases)
        { }

        public IQueryable<NewCase> FindByID(long id)
        {
            var db = GetDBContext();

            return db.NewCases.Where(n => n.currentIdCase == id);
        }
        
    }
}
