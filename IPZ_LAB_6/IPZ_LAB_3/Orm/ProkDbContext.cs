﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace IPZ_LAB_3.Orm
{
   public class ProkDbContext: DbContext
    {
        static ProkDbContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ProkDbContext>());
            
        }

        public DbSet<NewCase> NewCases { get; set; }
        
        public DbSet<Suspect> Sus { get; set; }
        public DbSet<Witnesses> Wit { get; set; }
        public DbSet<Name> Name { get; set; }
        public DbSet<CaseID> CaseID { get; set; }
        public DbSet<Evidence> Evidence{ get; set; }

        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new NewCaseConfiguration());
            modelBuilder.Configurations.Add(new WitnesseConfig());
            modelBuilder.Configurations.Add(new SuspectConfig());
            modelBuilder.Configurations.Add(new EvidenceConfig());
            modelBuilder.Configurations.Add(new NameConfiguration());
            modelBuilder.Configurations.Add(new AddressConfiguration());
            
        }

    }
}
