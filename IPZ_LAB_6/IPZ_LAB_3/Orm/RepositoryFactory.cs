﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_LAB_3.Orm
{
public static class RepositoryFactory
    {
    public static IWitnesseRepository MakeWitnesseRepository( ProkDbContext dbContext)
    {
        return new WitnesseRepository(dbContext);
    }
    public static INameRepository MakeNamesRepository(ProkDbContext dbContext)
    {
        return new NameRepository(dbContext);
    }

    public static IMenuRepository MakeMenuRepository(ProkDbContext dbContext)
    {
        return new MenuRepository(dbContext);
    }
    public static ISuspectRepositroy MakeSuspectRepositroy(ProkDbContext dbContext)
    {

        return new SuspectRepository(dbContext);
    }
    public static IEvidenceRepository MakeEvidenceRepository(ProkDbContext dbContext)
    {
        return new EvidenceRepository(dbContext);
    }
    }
}
