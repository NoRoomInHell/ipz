﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
namespace IPZ_LAB_3.Orm
{
    class AddressConfiguration: EntityTypeConfiguration<Address>
    {
        public AddressConfiguration()
        {
            HasKey(q => q.City);
            Property(q =>q.Street);
            Property(q =>q.House);
        }
    }
}
